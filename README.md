# Elliptec-Artiq

A python library for controlling ThorLabs Elliptec stages from artiq.

# Example program

The following program will step an elliptec rotation stage through angles 0 (home), 45 degrees, and 90 degrees.

```
from elliptec.motor import Motor
from elliptec.helper import get_serial_ports
import time

device = '/dev/ttyUSB0'
motor = Motor(device)

print(motor)

for i in range(10):
    motor.home()
    time.sleep(1.0)
    motor.move_absolute(45)
    time.sleep(1.0)
    motor.move_absolute(90)
    time.sleep(1.0)
```
