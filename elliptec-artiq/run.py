import argparse
import logging
import sys
import asyncio

from device import ElliptecController, LinearElliptecController

from sipyco.pc_rpc import Server
from sipyco import common_args

def get_argparser():
   parser = argparse.ArgumentParser(
      description="""Controller for ThorLabs elliptec motors.""")
   parser.add_argument("-d", "--device", default=None, help="Device address")
   parser.add_argument("-l", "--type", default=None, help="Type of device: linear or rotation")
   common_args.simple_network_args(parser, 3272)
   common_args.verbosity_args(parser)
   return parser

def main():
   args = get_argparser().parse_args()
   common_args.init_logger_from_args(args)

   if args.device is None:
      print("You need to supply a -d/--device argument. Use --help for more information.")
      sys.exit(1)

   async def run():
      print("Connecting to Elliptec device, ID=%s." % args.device)
      if args.type == 'linear':
         device = LinearElliptecController(args.device)
      elif args.type == 'rotation':
         device = ElliptecController(args.device)
      else:
         raise Exception("Unknown stage type.")    
      server = Server({"device": device}, args.host, args.port)
      await server.start(common_args.bind_address_from_args(args), args.port)
      try:
         await server.wait_terminate()
      finally:
         await server.stop()

   loop = asyncio.get_event_loop()
   try:
      loop.run_until_complete(run())
   except KeyboardInterrupt:
      pass
   finally:
      loop.close()

   if __name__ == "__main__":
      main()
