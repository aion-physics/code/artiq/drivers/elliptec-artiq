#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from elliptec.motor import Motor, LinearMotor

"""
Exposes an elliptec motor device to artiq

Attributes:
    motor (Motor): reference to the controlled serial motor.
"""
class ElliptecController:
    
    def __init__(self, device):
        self.motor = self._get_device(device)
        
    def _get_device(self, device):
        return Motor(device)
    
    def go_home(self):
        return self.motor.home()
    
    def move_absolute(self, value):
        return self.motor.move_absolute(value)
    
    def get_position(self):
        return self.motor.get_position()
    
    def optimise(self):
        self.motor.optimise_motor(1)
        
    def ping(self):
        return True
    
class LinearElliptecController(ElliptecController):
    
    def _get_device(self, device):
        return LinearMotor(device)