import serial
import sys
import time
from .cmd import get_, set_, mov_, opt_
from .errors import error_codes, is_error, is_busy

"""
An elliptec Motor device.

Attributes:

    range (float): The travel range of the motor.
        For rotation stages, this is specified in degrees.
        For linear stages, this is specified in mm.

    pulses (int): The number of encoder pulses per measurement unit.

    error_code (int): Last received error code.
"""
class Motor(serial.Serial):

    def __init__(self, port, baudrate=9600, bytesize=8, parity='N', timeout=10):
        try:
            super().__init__(port, baudrate=9600, bytesize=8, parity='N', timeout=10)
        except serial.SerialException:
            print('SerialException while attempting to open port %s' % port)
            sys.exit()

        if self.is_open:
            time.sleep(2)
            self.reset_input_buffer()
            self.flush()
            print('Connection established!')
            self.read_device_info()
            self.busy = False
            print(self)

    """
    Reads information about the motor from the device.

    Sends the `info` command, with the reply stored in `self.info`.
    """
    def read_device_info(self, addr='0'):
        self._send_command('info', '', get_, addr)
        
    """
    Performs the optimisation routine for the device.
    
    Searches for ideal frequency parameters for the specified motor ID.
    """
    def optimise_motor(self, motor, addr='0'):
        self._send_command('search_freq', str(motor), opt_, addr)
        self.check_status(addr=addr)
        self._send_command('save_motor_data', '', opt_, addr)
        self.check_status(addr=addr)
        
    def check_status(self, addr='0'):
        self._send_command('status', '', get_, addr)

    """
    Moves the device to the home location.
    
    Notes:
        For rotary stages, the data payload can indicate which way round to turn home.
        However, I haven't implemented that here.
    """
    def home(self, addr='0'):
        self.move(comm='home', data='0', addr=addr)
        
    """
    Moves the motor to the stated absolute position.
    
    Arguments:
        position (float): position in units of mm or degrees.
    """
    def move_absolute(self, position, addr='0'):
        self.move(comm='absolute', data=self.units_to_hex(position), addr=addr)

    """
    Moves the device.

    Note:
        The device will continue to send GS status info until it reaches the destination.
        Then it will send PO. See eg _DEVGET_POSITION.
    """
    def move(self, comm='home', data='0', addr='0'):
        self.status = self._send_command(comm, data, mov_, addr)
        return self.status

    def set_property(self, comm='', data='', addr='0'):
        self.status = self._send_command(comm, data, set_, addr)
        return self.status

    def get_(self, comm='status', data='', addr='0'):
        self.status = self._send_command(comm, data, get_, addr)
        return self.status
    
    def get_position(self, addr='0'):
        self.status = self._send_command('position', '', get_, addr)
        return self.status
    
    # def set_home(self, )

    """
    Constructs a packet for the specified command.

    Arguments:
        comm (string): command to build packet for.
        data (string): string data payload
        addr (string): address of destination device

    Returns:
        packet: constructed packet.
    """
    def _build_packet(self, comm, data, addr, comm_list):
        instruction = comm_list[comm]
        packet = addr.encode('utf-8') + instruction
        if data:
            packet += data.encode('utf-8')
        return packet

    """
    Sends a command to the device via the serial connection and reads the response.

    Returns: 
        parsed response
    """
    def _send_command(self, comm, data, comm_list, addr='0'):
        packet = self._build_packet(comm, data, addr, comm_list)
        #print('command: addr=%s comm=%s data=%s' % (addr, comm, data))
        print('Sending command: ' + packet.decode('utf-8'))
        self.write(packet)
        response = self.read_until(terminator=b'\n')
        return self._handle_reply(response)

    """
    Handles the reply from an elliptec motor.

    Arguments:
        msg (bytes): The response received from the motor.
    """
    def _handle_reply(self, msg):
        if (not msg.endswith(b'\r\n') or (len(msg) == 0)):
            print('Malformed response: \'%s\'' % msg)
            return None
        msg = msg.decode().strip()
        addr = msg[0]
        code = msg[1:3]
        if len(msg) > 3:
            content = msg[3:]
        else:
            content = ''
        try: 
            addr = int(addr, 16)
        except ValueError:
            raise ValueError('Invalid Address: %s' % msg[0])

        lookup = {
                'IN': self._parse_info,
                'PO': self._parse_position,
                'BO': self._parse_position,
                'GS': self._parse_status
            }

        try:
            strategy = lookup[code.upper()]
        except KeyError:
            strategy = self.parse_default

        return (code,strategy(addr, code, content))

    """
    Default strategy to parse messages
    """
    def _parse_default(self, addr, code, content):
        print("Unhandled message type. (" + code + "): " + content + ".")
        return content

    """
    Parse 'position' message to get device position.

    Notes:
        See the command _DEVGET_POSITION.

        The returned PO message contains the current 32-bit position
    """
    def _parse_position(self, addr, code, content):
        position = self.hex_to_units(content)
        print('Motor finished move. Current position=%f.' % position)
        return position

    """
    Parse `status` message from device.

    Notes:
        See the documentation for _DEVGET_STATUS.

        The message structure contains addr, code=GS and a 2 character content.
        The content specifies an error code which can be used to evaluate the device status.
    """
    def _parse_status(self, addr, code, content):
        error_code = int(content, 16)
        self.error_code = error_codes[error_code]
        if is_error(error_code):
            print("Motor in error state: %s" % error_codes[error_code])
        elif is_busy(error_code):
            print("Motor is busy.")
            self.busy = True
        else:
            print("Motor status OK.")
            self.busy = False
        return error_code

    """
    Parse 'information' message to extract device info.

    Notes:
        See the command _DEVGET_INFORMATION in the manual.
        
        The returned message is of the form:
            [0-2]: header, '<addr>in'
            [3-4]: model, used to identify device, eg 06 for EL06.
            [5-12]: device serial number
            [13-16]: year of manufacture
            [17-18]: firmware version
            [19-20]: hardware info, firmware+thread type.
            [21-24]: travel range, in mm or degrees.
            [25-32]: pulses/number of positions.

    """
    def _parse_info(self, addr, code, content):
        msg = 'A' + code + content
        info = {
            'model' : int(msg[3:5], 16),
            'serial' : msg[5:13],
            'year' : msg[13:17],
            'firmware' : int(msg[17:19], 16),
            'hardware' : msg[19:21],
            'range' : int(msg[21:25], 16),
            'pulses' : int(msg[25:33], 16)
            }
        self.info = info
        self.pulses = self.info['pulses']
        self.range = self.info['range']
        print('Received info packet from motor: %s' % msg)
        return info

    """ 
    Convert units to hexadecimal code.

    Note:
        For rotation stages, the units are specified in degrees.
        For linear translation stages, units are specified in mm.
    """
    def units_to_hex(self, quantity):
        val = hex(int(quantity*self.pulses/self.range))
        return val.replace('0x', '').zfill(8).upper()

    """
    Convert hexadecimal code to units.

    Note:
        For rotation stages, the units are specified in degrees.
        For linear translation stages, units are specified in mm.
    """
    def hex_to_units(self, hexval):
        val = (int(hexval,16)/self.pulses)*self.range
        return val

    def __str__(self):
        string = 'port : ' + self.port + '\n'
        for key in self.info:
            string += (key + ' : ' + str(self.info[key]) + '\n')            
        return string
    
"""
Represents a linear motor from the Thorlabs elliptec range.

Annoyingly, it seems to have a different conversion formula, compared to the 
rotation stages...
"""
class LinearMotor(Motor):
    
    """ 
    Convert units to hexadecimal code.

    Note:
        For rotation stages, the units are specified in degrees.
        For linear translation stages, units are specified in mm.
    """
    def units_to_hex(self, quantity):
        value = int(quantity * self.pulses)
        if value < 0:
            value = (1 << 32) + value
        val = hex(value)
        return val.replace('0x', '').zfill(8).upper()

    """
    Convert hexadecimal code to units.

    Note:
        For rotation stages, the units are specified in degrees.
        For linear translation stages, units are specified in mm.
    """
    def hex_to_units(self, hexval):
        integer = int(hexval,16)
        if (integer & (1 << 31)) > 0:
            integer = integer - (1<<32)
        return integer/self.pulses
