error_codes = {
	0 : 'Status OK',
	1 : 'Communication Timeout',
	2 : 'Mechanical Timeout',
	3 : 'Command Error',
	4 : 'Value Out of Range',
	5 : 'Module Isolated',
	6 : 'Module Out of Isolation',
	7 : 'Initialisation Error',
	8 : 'Thermal Error',
	9 : 'Busy',
	10: 'Sensor Error',
	11: 'Motor Error',
	12: 'Out of Range',
	13: 'Over Current Error',
}

"""
Returns true if a given code corresponds to an error.
"""
def is_error(code):
	return (code != 0 and code != 9)

def is_busy(code):
    return (code == 9)

if __name__ == '__main__':
	print(error_codes)