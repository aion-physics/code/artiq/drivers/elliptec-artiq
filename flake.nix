{
  description = "AION controller for ThorLabs Elliptec motors";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  inputs.sipyco.url = github:m-labs/sipyco;  
  inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, sipyco }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
    in rec {
	
      packages.x86_64-linux = {
        elliptec-artiq = pkgs.python3Packages.buildPythonPackage {
          pname = "elliptec-artiq";
          version = "0.1";
          src = self;
          propagatedBuildInputs = [ sipyco.packages.x86_64-linux.sipyco ];
        };
      };

      defaultPackage.x86_64-linux = pkgs.python3.withPackages(ps: [ packages.x86_64-linux.elliptec-artiq ]);

      devShell.x86_64-linux = pkgs.mkShell {
        name = "elliptec-artiq-dev-shell";
        buildInputs = [
          (pkgs.python3.withPackages(ps: [ sipyco.packages.x86_64-linux.sipyco packages.x86_64-linux.elliptec-artiq ]))
        ];
      };
    };
}
