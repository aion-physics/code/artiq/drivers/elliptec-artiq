#!/usr/bin/env python3

import sys
from setuptools import setup
from setuptools import find_packages

setup(
   name="elliptec_artiq",
   version="0.1",
   description="AION controller for ThorLabs Elliptec motors",
   author="Elliot Bentine",
   author_email="elliot.bentine@physics.ox.ac.uk",
   url="https://gitlab.com/aion-physics/code/artiq/drivers/elliptec-artiq",
   download_url="https://gitlab.com/aion-physics/code/artiq/drivers/elliptec-artiq",
   packages=find_packages(),
   install_requires=[],
   entry_points={
      "console_scripts": []
   },
   license="LGPLv3+",
)